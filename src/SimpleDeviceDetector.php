<?php

namespace Nexweb\SimpleDeviceDetector;

class SimpleDeviceDetector
{
    const FEATURE_PHONE = 'feature_phone';
    const SMART_PHONE = 'smart_phone';
    const TABLET = 'tablet';
    const DESKTOP = 'desktop';

    private $type;

    /**
     * @param array $server Array of $_SERVER variables
     */
    public function detect($server)
    {
        if (!is_array($server)) {
            throw new \InvalidArgumentException('$server must be an array');
        }

        $userAgent = isset($server['HTTP_USER_AGENT']) ? $server['HTTP_USER_AGENT'] : '';

        // Opera mini will change the user agent, but copy the original in X_OPERAMINI_PHONE_UA
        if (!empty($server['HTTP_X_OPERAMINI_PHONE_UA'])) {
            $userAgent = $server['HTTP_X_OPERAMINI_PHONE_UA'];
        }

        $accept = isset($server['HTTP_ACCEPT']) ? $server['HTTP_ACCEPT'] : '';

        // if ipad or android not mobile, then it's a tablet
        // add xoom because some version still have the 'mobile' keyword
        $isTablet = stripos($userAgent, 'iPad') !== false ||
                    stripos($userAgent, 'xoom') !== false ||
                    (stripos($userAgent, 'Android') !== false && stripos($userAgent, 'Mobile') === false);

        $isSmartPhone = stripos($userAgent, 'iPhone') ||
                        stripos($userAgent, 'Android') ||
                        stripos($userAgent, 'webOS') ||
                        stripos($userAgent, 'iPod') ||
                        stripos($userAgent, 'windows phone 8') ||
                        stripos($userAgent, 'BB10');

        $isMobilePhone = stripos($accept, 'application/vnd.wap.xhtml+xml') !== false
            || stripos($accept, 'text/vnd.wap.wml') !== false
            || stripos($userAgent, 'sony') !== false
            || stripos($userAgent, 'symbian') !== false
            || stripos($userAgent, 'nokia') !== false
            || stripos($userAgent, 'samsung') !== false
            || stripos($userAgent, 'mobile') !== false
            || stripos($userAgent, 'windows ce') !== false
            || stripos($userAgent, 'epoc') !== false
            || stripos($userAgent, 'opera mini') !== false
            || stripos($userAgent, 'nitro') !== false
            || stripos($userAgent, 'j2me') !== false
            || stripos($userAgent, 'midp-') !== false
            || stripos($userAgent, 'cldc-') !== false
            || stripos($userAgent, 'netfront') !== false
            || stripos($userAgent, 'mot') !== false
            || stripos($userAgent, 'up.browser') !== false
            || stripos($userAgent, 'up.link') !== false
            || stripos($userAgent, 'audiovox') !== false
            || stripos($userAgent, 'blackberry') !== false
            || stripos($userAgent, 'ericsson,') !== false
            || stripos($userAgent, 'panasonic') !== false
            || stripos($userAgent, 'philips') !== false
            || stripos($userAgent, 'sanyo') !== false
            || stripos($userAgent, 'sharp') !== false
            || stripos($userAgent, 'sie-') !== false
            || stripos($userAgent, 'portalmmm') !== false
            || stripos($userAgent, 'blazer') !== false
            || stripos($userAgent, 'avantgo') !== false
            || stripos($userAgent, 'danger') !== false
            || stripos($userAgent, 'palm') !== false
            || stripos($userAgent, 'series60') !== false
            || stripos($userAgent, 'palmsource') !== false
            || stripos($userAgent, 'pocketpc') !== false
            || stripos($userAgent, 'smartphone') !== false
            || stripos($userAgent, 'rover') !== false
            || stripos($userAgent, 'ipaq') !== false
            || stripos($userAgent, 'au-mic,') !== false
            || stripos($userAgent, 'alcatel') !== false
            || stripos($userAgent, 'ericy') !== false
            || stripos($userAgent, 'up.link') !== false
            || stripos($userAgent, 'vodafone/') !== false
            || stripos($userAgent, 'wap1.') !== false
            || stripos($userAgent, 'wap2.') !== false;

        if ($isTablet) {
            $this->type = self::TABLET;

        } elseif ($isMobilePhone) {
            if ($isSmartPhone) {
                $this->type = self::SMART_PHONE;
            } else {
                $this->type = self::FEATURE_PHONE;
            }
        } else {
            $this->type = self::DESKTOP;
        }
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function isTablet()
    {
        return $this->type == self::TABLET;
    }

    /**
     * @return bool
     */
    public function isDesktop()
    {
        return $this->type == self::DESKTOP;
    }

    /**
     * @return bool
     */
    public function isSmartPhone()
    {
        return $this->type == self::SMART_PHONE;
    }

    /**
     * @return bool
     */
    public function isFeaturePhone()
    {
        return $this->type == self::FEATURE_PHONE;
    }
}
