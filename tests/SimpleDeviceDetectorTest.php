<?php

class SimpleDeviceDetectorTest extends \PHPUnit\Framework\TestCase
{

    public function testInvalidParameters()
    {
        $detector = new \Nexweb\SimpleDeviceDetector\SimpleDeviceDetector();

        $this->expectException(InvalidArgumentException::class);

        $detector->detect(null);
        $detector->detect('User-agent');
    }

    public function testDetectDefault()
    {
        $detector = new \Nexweb\SimpleDeviceDetector\SimpleDeviceDetector();
        $detector->detect(['HTTP_USER_AGENT' => 'Non-existent user agent']);
        $detector->detect([]);

        // No exception returned
        $this->assertTrue(true);
    }

    public function testDetectDesktop()
    {
        $userAgent = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
        $server = array('HTTP_USER_AGENT' => $userAgent);

        $detector = new \Nexweb\SimpleDeviceDetector\SimpleDeviceDetector();
        $detector->detect($server);

        $this->assertEquals($detector->getType(), \Nexweb\SimpleDeviceDetector\SimpleDeviceDetector::DESKTOP);
        $this->assertTrue($detector->isDesktop());
        $this->assertFalse($detector->isTablet());
        $this->assertFalse($detector->isSmartPhone());
        $this->assertFalse($detector->isFeaturePhone());
    }

    public function testDetectIPad()
    {
        $userAgent = 'Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.10';
        $server = array('HTTP_USER_AGENT' => $userAgent);

        $detector = new \Nexweb\SimpleDeviceDetector\SimpleDeviceDetector();
        $detector->detect($server);

        $this->assertEquals($detector->getType(), \Nexweb\SimpleDeviceDetector\SimpleDeviceDetector::TABLET);
        $this->assertFalse($detector->isDesktop());
        $this->assertTrue($detector->isTablet());
        $this->assertFalse($detector->isSmartPhone());
        $this->assertFalse($detector->isFeaturePhone());
    }

    public function testDetectIPhone()
    {
        $userAgent = 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25';
        $server = array('HTTP_USER_AGENT' => $userAgent);

        $detector = new \Nexweb\SimpleDeviceDetector\SimpleDeviceDetector();
        $detector->detect($server);

        $this->assertEquals($detector->getType(), \Nexweb\SimpleDeviceDetector\SimpleDeviceDetector::SMART_PHONE);
        $this->assertFalse($detector->isDesktop());
        $this->assertFalse($detector->isTablet());
        $this->assertTrue($detector->isSmartPhone());
        $this->assertFalse($detector->isFeaturePhone());
    }

    public function testOperaMini()
    {
        $operaUserAgent = 'Opera/9.80 (Android; Opera Mini/30.0.2254/77.131; U; en) Presto/2.12.423 Version/12.16';
        $iPhoneUserAgent = 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25';
        $server = array(
            'HTTP_USER_AGENT' => $operaUserAgent,
            'HTTP_X_OPERAMINI_PHONE_UA' => $iPhoneUserAgent
        );

        $detector = new \Nexweb\SimpleDeviceDetector\SimpleDeviceDetector();
        $detector->detect($server);

        $this->assertEquals($detector->getType(), \Nexweb\SimpleDeviceDetector\SimpleDeviceDetector::SMART_PHONE);
        $this->assertFalse($detector->isDesktop());
        $this->assertFalse($detector->isTablet());
        $this->assertTrue($detector->isSmartPhone());
        $this->assertFalse($detector->isFeaturePhone());
    }

}